package com.example.brightcove;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokemonApplication {
//	private static final Logger log = LoggerFactory.getLogger(PokemonApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PokemonApplication.class, args);
	}
}
